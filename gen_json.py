#!/usr/bin/python
# Copyright (c) 2018, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Python DS notebook ZApp description generator."""

import json
import sys
import os

APP_NAME = 'pydatasci'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'memory_limit': {
        'value': 6 * (1024**3),
        'description': 'Memcached memory limit (bytes)'
    },
    'core_limit': {
        'value': 2,
        'description': 'Number of cores to allocate'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

IMAGE = REPOSITORY + '/pydatasci:' + VERSION
IMAGE_GPU = REPOSITORY + '/pydatasci-gpu:' + VERSION

def nb_service(memory_limit, core_limit, image, gpu=False):
    """
    :rtype: dict
    """
    service = {
        'name': "py-notebook",
        'image': image,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [
            {
                "name": "Jupyter Notebook interface",
                "port_number": 8888,
                "protocol": "tcp",
                "url_template": "http://{ip_port}{proxy_path}",
                "proxy": True
            },
            {
                "name": "Tensorboard",
                "port_number": 6006,
                "protocol": "tcp",
                "url_template": "http://{ip_port}/"
            }
        ],
        'environment': [],
        'volumes': [],
        'command': '/usr/local/bin/start_notebook.sh',
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1
    }
    if gpu:
        service['labels'] = ['gpu']
        service['environment'] = [['NVIDIA_VISIBLE_DEVICES','all']]

    return service


if __name__ == '__main__':
    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            nb_service(options["memory_limit"]["value"], options["core_limit"]["value"], IMAGE)
        ]
    }

    json.dump(app, open("pydatasci.json", "w"), sort_keys=True, indent=4)

    app['services'][0]['labels'] = ['labs']
    json.dump(app, open("pydatasci-malis.json", "w"), sort_keys=True, indent=4)

    app['services'][0]['resources']['memory']['min'] = 10 * (1024**3)
    app['services'][0]['resources']['memory']['max'] = 10 * (1024**3)
    app['services'][0]['resources']['cores']['min'] = 4
    app['services'][0]['resources']['cores']['max'] = 4
    app['services'][0]['labels'] = ['labs']
    json.dump(app, open("pydatasci-aml.json", "w"), sort_keys=True, indent=4)

    app = {
        'name': APP_NAME + '-gpu',
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            nb_service(options["memory_limit"]["value"], options["core_limit"]["value"], IMAGE_GPU, gpu=True)
        ]
    }

    json.dump(app, open("pydatasci-gpu.json", "w"), sort_keys=True, indent=4)

    with open('images', 'w') as fp:
        fp.write(IMAGE + '\n')
        fp.write(IMAGE_GPU + '\n')

    print("ZApp written")

